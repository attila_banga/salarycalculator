import { Directive, ElementRef, Input, OnInit, HostListener } from '@angular/core';
import { ProfilComponent } from './profil/profil.component';

@Directive({
  selector: '[appArrows]'
})
export class ArrowsDirective implements OnInit{
  @Input() increase: boolean;
  @Input() value: number;

  constructor(private elementRef: ElementRef) { }

  ngOnInit() {
    
  }

  @HostListener('mouseup') onmouseup() {
    if(this.increase) {
      ProfilComponent.cost += this.value;
    }
    else {
      ProfilComponent.cost -= this.value;
    }
  }
}
