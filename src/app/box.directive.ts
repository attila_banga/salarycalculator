import { Directive, ElementRef, Input, OnInit, HostListener } from '@angular/core';
import { ProfilComponent } from './profil/profil.component';

@Directive({
  selector: '[appBox]'
})
export class BoxDirective implements OnInit{
  @Input() value: number;
  @Input() checked: boolean;
  @Input() box: string;
  @Input() img: string;


  constructor(private elementRef: ElementRef) { }

  ngOnInit() {
    ProfilComponent.cost = 0;
  }

  @HostListener('change') onChange(){
    this.checked = !this.checked;
    switch(this.box) {
      case "levels": {
        if(this.checked) {
          ProfilComponent.img = this.img;
          ProfilComponent.multiplier = this.value;
        }
        else {
          ProfilComponent.img = "/assets/image1.jpg";
          ProfilComponent.multiplier = 1;
        }
        break;
      }
      default: {
        if(this.checked) {
          ProfilComponent.cost += this.value;
        }
        else {
          ProfilComponent.cost -= this.value;
        }
        break;
      }
    }
}
}
