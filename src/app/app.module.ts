import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { TablesComponent } from './tables/tables.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { ProfilComponent } from './profil/profil.component';
import { BoxDirective } from './box.directive';
import { ArrowsDirective } from './arrows.directive';

@NgModule({
  declarations: [
    AppComponent,
    TablesComponent,
    CheckboxComponent,
    ProfilComponent,
    BoxDirective,
    ArrowsDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
