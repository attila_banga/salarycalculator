import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';
import { BoxDirective } from '../box.directive'

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.css']
})
export class TablesComponent implements OnInit {

  private programmers$: Object;
  private boxD: BoxDirective;

  constructor(private data: DataService) {
    this.data.getProgrammers().subscribe(
      data => this.programmers$ = data
    );
  }

  ngOnInit() {
    
  }

}
