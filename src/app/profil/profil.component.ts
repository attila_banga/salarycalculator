import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  private programmers$: Object;
  public static cost: number = 0;
  public static multiplier: number = 1;
  public static img: string = "/assets/image1.jpg";

  constructor(private data: DataService) {
    
  }
  
  ngOnInit() {
    this.data.getProgrammers().subscribe(
      data => this.programmers$ = data
    );

  }

  get getStaticCost() {
    return ProfilComponent.cost;
  }

  get getMultiplier() {
    return ProfilComponent.multiplier;
  }

  get getImage() {
    return ProfilComponent.img;
  }
}
